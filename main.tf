provider "aws" {
  region  = "ap-northeast-1"
  profile = "quangdvn-aws"
}

variable "environment" {
  type        = string
  description = "Environment"
  default     = "dev"
}

# variable "vpc_cidr_block" {
#   type        = string
#   description = "VPC CIDR Block"
# }

variable "cidr_blocks" {
  description = "CIDR Blocks for VPC and Subnet"
  type = list(object({
    cidr_block = string
    name       = string
  }))
}

variable "avail_zone" {
  description = "Availablity zone"
  type        = string
}

resource "aws_vpc" "dev-vpc" { # Create new
  cidr_block = var.cidr_blocks[0].cidr_block
  tags = {
    Name    = var.cidr_blocks[0].name,
    vpc-env = var.environment
  }
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id            = aws_vpc.dev-vpc.id
  cidr_block        = var.cidr_blocks[1].cidr_block
  availability_zone = var.avail_zone
  tags = {
    Name = var.cidr_blocks[1].name,
  }
}

# data "aws_vpc" "existing-vpc" { # Get data
#   default = true
# }

# resource "aws_subnet" "terra-subnet-1" {
#   vpc_id            = data.aws_vpc.existing-vpc.id
#   cidr_block        = "172.31.48.0/20"
#   availability_zone = "ap-northeast-1a"
#   tags = {
#     Name = "terra-subnet-1"
#   }
# }

output "dev-vpc-id" { # Like return value in function
  value = aws_vpc.dev-vpc.id
}

output "dev-subnet-id" { # Like return value in function
  value = aws_subnet.dev-subnet-1.id
}
